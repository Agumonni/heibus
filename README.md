# HEIBUS

## GIT

How to get the code

first you will need to install git for your raspberry pi

```
sudo apt-get install git
```


how to git:

Cloning a repository from gitlab:
first choose the location where you are gonna clone the files.
/home/pi is fine

```
git clone https://gitlab.com/Agumonni/heibus.git
```

after this move to the folder with the cd command

<b> ALWAYS <b> when testing the programs or modifying the old code make sure you have the newest version of the code

```
git pull
```

When creating the code after the changes you have made add the files to the repository

```
git add .
```

After adding the files to the repository create a descriptive commit message

```
git commit -m "Descriptive message about the changes you have made"
```

After this you are ready to push the files  to the cloud

```
git push
```
After this you have now successfully pushed the files to git.

in case of problems contac the help line.

number to call and cry about your issues:
Mikko Leppäniemi +358 40 0691860


## Running the code

first install the python programs


```
sudo apt-get update
 
sudo apt-get install python3 

pip3 install opencv-python
```


to run the code go to the directory 
cd heibus\2.13inch_e-Paper_HAT-code\raspberrypi\python

and run the code with 

```
sudo python3 main.py
```


## Running scripts as services

If you do not want to run the script individually all the time again and again, you can run it as a service.
At the moment we are using supervisorctl to run the script in the background, you can install supervisor with
```
sudo apt install supervisor
```

After installation, you have to configure a service in /etc/supervisor/conf.d/ directory, name it as scriptname.conf
Inside the .conf file, you need to configure ATLEAST the program name, directory and the command to run

```
[program:script]
directory=Root of the python script you want to run, ie. /home/admin/Virtuaalikaveri/backend/Script
command=python Script.py
autostart=true
autorestart=true
```

When the service is configured, all you have to do is
```
sudo supervisorctl update
```
You need to update everytime you make a change to the .conf file, when update is done, you can control your service with
```
sudo supervisorctl [start | stop | restart | status] script
```

For logs, you can use
```
tail -f /var/log/supervisor/supervisord.log
```
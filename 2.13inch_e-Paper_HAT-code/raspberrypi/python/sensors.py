import board
import busio
import adafruit_bme280
i2c = busio.I2C(board.SCL, board.SDA)
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)

def get_temp():
    return float("%.01f"%bme280.temperature)

def get_humidity():
    return float("%.01f"%bme280.humidity)

def get_pressure():
    return "{0:.0f}".format(bme280.pressure)


